/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoublyLinkedList;

import Interface.List;
import java.util.Iterator;
import java.util.ListIterator;
/**
 *
 * @author LestherCarranza
 */
public class DoublyLinkedList<E> implements List<E>,Iterable<E>{
    private Node<E> first,last;
    private int efective;

    public DoublyLinkedList() {
        this.first = null;
        this.last = null;
        this.efective = 0;
    }

    @Override
    public boolean addFirst(E element) {
        Node<E> nodo = new Node<>(element);
        if(element==null) return false;
        else if(isEmpty())
            first=last=nodo;
        else{
            nodo.setNext(first);
            first.setPrevious(nodo);
            first=nodo;
            
        }
        efective++;
        return true;
        
    }

    @Override
    public boolean addLast(E element) {
        Node<E> nodo= new Node<>(element);
        if(element==null) return false;
        else if(isEmpty())
            first=last=nodo;
        else{
            //nodo.setNext(last);
            last.setNext(nodo);
            nodo.setPrevious(last);
            last=nodo;
            
        }
        efective++;
        return true;
    }

    @Override
    public boolean removeFirst() {
     if(isEmpty())
            return false;
        else if(first==last)
            first=last=null;
        else{
            first=first.getNext();
            first.getPrevious().setNext(null);
            first.setPrevious(null);
        }
        efective--;
        return true;
    }   

    @Override
    public boolean removeLast() {
        if(isEmpty())
            return false;
        else if(first==last)
            first=last=null;
        else{
            last=last.getPrevious();
            last.getNext().setPrevious(null);
            last.setNext(null);
        }
        efective--;
        return true;
        
    }

    @Override
    public E getFirst() {
        return first.getData();
    }

    @Override
    public E getLast() {
        return last.getData();
    }

    @Override
    public boolean contains(E element) {
        if(isEmpty()){
            return false;
        }
        for(Node<E> i=first;i!=null;i=i.getNext()){
            if(i.getData().equals(element)){
                return true;        
            }
        }
        return false;
    }

    public boolean insert(E element,int index) {
        Node<E> nodo= new Node<>(element);
        if(element==null)return false;
        else if(index<0||index>efective-1) throw new IndexOutOfBoundsException();
        else if(index==0){ addFirst(element);
        return true;}
        else if(index==efective-1){ addLast(element);
        
        return true;
        }
        else{
            int contador=0;
            
            for(Node<E> viajero=first;viajero!=null;viajero=viajero.getNext()){
                if(contador==index-1){
                    nodo.setNext(viajero.getNext());
                    viajero.setNext(nodo);
                    //contador++;
                    
                }
                contador++;
                
            }
            
    }
        efective++;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return first == null && last==null;
    }

    @Override
    public int size() {
        return efective;
    }

    @Override
    public boolean remove(int index) {
        if(index>efective-1||isEmpty())
            return false; 
        int num=0;
        for(Node<E> i=first;i!=null;i=i.getNext()){
            if(index==num){
                if(index==0){
                    removeFirst();
                    return true;
                 //return i.getData();
                }
                i.getPrevious().setNext(i.getNext());
                i.getNext().setPrevious(i.getPrevious());
                i.setNext(null);
                i.setPrevious(null);
                //return i.getData();
                return true;
            }
            num++;
        }
        return false;
    
    }

    public List<E> slicing(int start, int end) {
        DoublyLinkedList<E> nueva= new DoublyLinkedList<>();
        int contador=0;
        if(start<0||start>end||start>efective||end>efective){
            throw new IndexOutOfBoundsException("Limites incorrectos");
            
        }else if(isEmpty()){
            throw new NullPointerException("Lista vacia");
        }else{
            for(Node<E> i=first;i!=null;i=i.getNext()){
                if(contador>=start && contador<end){
                    nueva.addLast(i.getData());
                    //contador++;
                        
                    }
                contador++;
                    
                    
                }
            }
        return nueva;
        }
    

    public List<E> reverse() {
        DoublyLinkedList nueva= new DoublyLinkedList<>();
        for(Node<E> i=last;i!=null;i=i.getPrevious()){
            nueva.addLast(i.getData());
            
        }
        return nueva;
    }

    public E getElement(int i) {
        if(isEmpty()){
           throw new IndexOutOfBoundsException();
    }   else if(i<0){
        throw new IndexOutOfBoundsException("indice negativo");
    }   else if(i>efective-1){
        throw new IndexOutOfBoundsException("Limite superado");
    }   else{
        int contador=0;
        for(Node<E> e=first;e!=null;e=e.getNext()){
            if(contador!=i){
                contador++;
            }else{
                return e.getData();
            }
        }
            
        }
        throw new NullPointerException();//return null;
    }
    
    @Override
    public String toString() {
        StringBuilder formato;
        formato=new StringBuilder("[");
        for(Node<E> i=first;i!=null;i=i.getNext()){
            formato.append(i.getData().toString());
            if(i.getNext()!=null)
                formato.append(",");
            else
                formato.append("]");
                        
        
        }
        return formato.toString();
    
}
    @Override
    public boolean equals(Object obj) {
      if(obj==null)return false;
      if(obj instanceof DoublyLinkedList){
          DoublyLinkedList other= (DoublyLinkedList)obj;
          if(efective==other.efective){
              Node<E> j= other.last;
              for(Node<E> i=first;i!=null;i=i.getNext()){
                  if(i.getData().equals(j.getData()))
                      j=j.getNext();
                  else
                      return false;
              }
              return true;
              
          }else{
          return false;}
      }
      return false;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<E> it= new Iterator<E>(){
            private Node<E> p=first;
            @Override
            public boolean hasNext() {
                return p!=null;
            }

            @Override
            public E next() {
              E data=p.getData();
              p=p.getNext();
              return data;
            }
            
            
            
        };
        return it;
    }
    public ListIterator<E> listIterator(int index){
        ListIterator<E> it= new ListIterator<E>(){
            private Node<E> p= getNode(index);
            private int currentIndex=index;
            @Override
            public boolean hasNext() {
                return p!=null;
            }

            @Override
            public E next() {
                E data=p.getData();
                currentIndex++;
                p=p.getNext();
                return data;
            }

            @Override
            public boolean hasPrevious() {
               return p!=null;
            }

            @Override
            public E previous() {
                E data=p.getData();
                currentIndex--;
                p=p.getPrevious();
                return data;
            }

            @Override
            public int nextIndex() {
                return currentIndex+1;
                
            }

            @Override
            public int previousIndex() {
                return currentIndex-1;
            }       

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void set(E e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void add(E e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        return it;
    }
    private Node<E> getNode(int index){
        if(index==0){
            return first;
        }else if(index==efective-1){
            return last;
        }
        int contador=1;
        for(Node<E> viajero=first.getNext();viajero!=null;viajero=viajero.getNext()){
            if(contador==index){
                return viajero;
            }contador++;
        }
        return null;
        
    }
    
}
