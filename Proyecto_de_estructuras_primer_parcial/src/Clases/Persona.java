/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LestherCarranza
 */
public class Persona {
    private String nombre;
    private int edad;
    private String cedula;
    private String sexo;
    private String Nacionalidad;
    private int etiqueta;
    private boolean Capacidad;
    public Persona(){
        
    }
    public void setEtiqueta(int etiqueta){
        this.etiqueta=etiqueta;
    }
    public int getEtiqueta(){
        return this.etiqueta;
    }
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return this.nombre;
    }
    public void setEdad(int edad){
           this.edad=edad;
    }
    public int getEdad(){
        return this.edad;
    }
    public void setCedula(String cedula){
        this.cedula=cedula;
    }
    public String getCedula(){
        return this.cedula;
    }
    public void setSexo(String sexo){
        this.sexo=sexo;
    }
    public String getSexo(){
        return this.sexo;
    }
    public void setNacionalidad(String nacionalidad){
        this.Nacionalidad=nacionalidad;
    }
    public String getNacionalidad(){
        return this.Nacionalidad;
    }
    public boolean isTerceraEdad(){
        return edad>=60;
    }
    public void setCapacidad(boolean capacidad){
        this.Capacidad=capacidad;
    }
    public boolean getCapacidad(){
        return this.Capacidad;
    }
}
