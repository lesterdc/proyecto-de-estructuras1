/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LestherCarranza
 */
public class Registro_Migratorio {
       private String codigo;
       private Persona persona;
       private String Tipo_Movilizacion;
       private String fecha;
       private String Destino;
       private String Punto_Salida;
       private String Via_Trasporte;
       private String Pais_procedencia; //es diferente de la nacionalidad de la persona
       public Registro_Migratorio(){
           
       }
       public void setCodigo(String codigo){
           this.codigo=codigo;
       }
       public String getCodigo(){
           return this.codigo;
       }
       public void setMovilizacion(String tipo){
           this.Tipo_Movilizacion=tipo;
       }
       public String getMovilizacion(){
           return this.Tipo_Movilizacion;
       }
       public void setFecha(String fecha){
           this.fecha=fecha;
       }
       public String getFecha(){
           return this.fecha;
       } 
       public void setDestino(String destino){
           this.Destino=destino;
       }
       public String getDestino(){
           return this.Destino;
       }
       public void setSalida(String salida){
           this.Punto_Salida=salida;
       }
       public String getSalida(){
           return this.Punto_Salida;
       }
       public void setTrasporte(String trasporte){
           this.Via_Trasporte=trasporte;
       }
       public String getTrasporte(){
           return this.Via_Trasporte;
       }
       public void setPais_pro(String pais){
           this.Pais_procedencia=pais;
       }
       public String getPais(){
           return this.Pais_procedencia;
       }
       public void setPersona(Persona persona){
           this.persona=persona;
       }
       public Persona getPersona(){
           return this.persona;
       }
}
