/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

/**
 *
 * @author LestherCarranza
 * @param <E>
 */
public interface List<E> {
    
    
    int size();
    boolean isEmpty();
    boolean addFirst(E element);
    boolean addLast(E element);
    boolean removeFirst();
    boolean removeLast();
    boolean remove(int i);
    boolean insert(E element, int i);
    boolean contains(E element);
    E getElement(int i);
    E getFirst();
    E getLast();
}
