/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registros;

import Clases.Persona;
import DoublyLinkedList.DoublyLinkedList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author LestherCarranza
 */
public class Registro {
    public static DoublyLinkedList<Persona> lista=new DoublyLinkedList<>();
    public static void muestraContenido(String archivo) throws FileNotFoundException, IOException{
        String cadena;
        String pers="person";
        int a=0;
        FileReader f= new FileReader(archivo);
        try (BufferedReader b = new BufferedReader(f)) {
            while((cadena=b.readLine())!=null){
                String[] Elementos=cadena.split(",");
                pers=pers.substring(0,5) + a;
                Persona per=new Persona();
                    per.setNombre(Elementos[0]);
                    int numEdad=Integer.parseInt(Elementos[1]);
                    per.setEdad(numEdad);
                    per.setCedula(Elementos[2]);
                    per.setSexo(Elementos[3]);
                    per.setNacionalidad(Elementos[4]);
                    if("true".equals(Elementos[5]))
                        per.setCapacidad(true);
                    else 
                        per.setCapacidad(false);
                //System.out.println(cadena);//en vez de imprimir hacer operaciones con esto
                lista.addFirst(per);
                a++;
            }
        }
    }
    //en la clase que lo llame se lo pone con throws IOEXception
    public static void crearArchivo(DoublyLinkedList<Persona> lista1) throws IOException{
        FileWriter flwriter=null;
        try{
            flwriter = new FileWriter("C:/Users/Pc/Desktop/Archivo_Procesado.csv");
            try (BufferedWriter bfwriter = new BufferedWriter(flwriter)) {
                for(Persona persona: lista1){
                    bfwriter.write(persona.getNombre()+","+persona.getCedula()+","+persona.getNacionalidad()+","+persona.getSexo());
                }
            }
            System.out.println("Archivo creado satisfactoriamente..");
        }catch (IOException e){
        }finally{
            if(flwriter != null){
                try{
                    flwriter.close();
                }catch(IOException e){
                }
            }
        }
    }
}
