/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registros;

import Clases.Persona;
import Clases.Registro_Migratorio;
import DoublyLinkedList.DoublyLinkedList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author LestherCarranza
 */
public class ColaDeOrdenAsignacion {
    public static DoublyLinkedList<Persona> personaOrdenEdad(DoublyLinkedList<Persona> persona){
        DoublyLinkedList<Persona> nueva=new DoublyLinkedList<>();
        PriorityQueue<Persona> cola=new PriorityQueue<>((Persona p1, Persona p2)->p2.getEtiqueta()-p1.getEtiqueta());
        while(!persona.isEmpty()){
            Persona p=persona.getFirst();
            if(p.isTerceraEdad()&&p.getCapacidad()){
                p.setEtiqueta(3);
            }else if(p.getCapacidad()){
                p.setEtiqueta(2);
            }
            else if(p.isTerceraEdad()){
                p.setEtiqueta(1);
            }
            else{
                p.setEtiqueta(0);
            }
            persona.removeFirst();
            cola.offer(p);
        }
        while(!cola.isEmpty()){
            Persona pr=cola.poll();
            nueva.addLast(pr);
        }
        return nueva;
    }
    //Esta sigue en prueba
    public static List<Registro_Migratorio> Asignacion_Turno(DoublyLinkedList<Persona> persona,Queue<Registro_Migratorio> turnos){
            List<Registro_Migratorio> nueva=new LinkedList<>();
            while(!persona.isEmpty()){
                Persona p=persona.getFirst();
                persona.removeFirst();
                if(turnos.peek().getPersona()==null){
                    Registro_Migratorio m=turnos.poll();
                    m.setPersona(p);
                    turnos.offer(m);
                }
            }
            nueva.addAll(turnos);
            return nueva;
        }
}
