/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaCircular;

/**
 *
 * @author LestherCarranza
 */
public class Node<E> {
    
    private E data;
    private Node<E> prev, next;
    
    public Node(E element){
        this.data = element;
        this.next = null;
    }
    
    public E getData(){
        return this.data;
    }
    
    public Node<E> getNext(){
        return this.next;
    }
    
    public void setNext(Node<E> next){
        this.next = next;
    }
    public Node<E> getPrev(){
        return this.prev;
    }
    
    public void setPrev(Node<E> prev){
        this.prev = prev;
    }
    
}
