/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaCircular;

import Interface.List;
import java.util.Iterator;
import java.util.Objects;

/**
 *
 * @author LestherCarranza
 */
public class DoublyLinkedCircularList<E> implements List<E>, Iterable<E>{
    
    private Node<E> first;
    private int size;
    
    public DoublyLinkedCircularList(){
        
        this.first = null;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public boolean addFirst(E element) {
        
        if(element == null)
            return false;
        
        Node<E> newNode = new Node<>(element);
        
        if(isEmpty()){
            
            newNode.setNext(newNode);
            newNode.setPrev(newNode);
            first = newNode;
            
        } else{
            
            newNode.setPrev(first.getPrev());
            first.getPrev().setNext(newNode);
            first.setPrev(newNode);
            newNode.setNext(first);
            first = newNode;
        }
        
        size++;
        return true;
    }

    @Override
    public boolean addLast(E element) {
        
        if(element == null)
            return false;
        
        Node<E> newNode = new Node<>(element);
        
        if(isEmpty()){
            
            newNode.setNext(newNode);
            newNode.setPrev(newNode);
            first = newNode;
            
        } else {
            
            first.getPrev().setNext(newNode);
            newNode.setPrev(first.getPrev());
            first.setPrev(newNode);
            newNode.setNext(first);
        }
        
        size++;
        return true;
    }

    @Override
    public boolean removeFirst() {
        
        if(isEmpty())
            return false;
        
        if(first.getNext() == first)
            first = null;
        else{
            
            first.getPrev().setNext(first.getNext());
            first.getNext().setPrev(first.getPrev());
            first.setPrev(null);
            Node<E> newFirst = first.getNext();
            first.setNext(null);
            first = newFirst;
        }
        
        size--;
        return true;
    }

    @Override
    public boolean removeLast() {
        
        if(isEmpty())
            return false;
        
        if(first.getNext() == first)
            first = null;
        else{
            
            first.getPrev().setNext(null);
            first.setPrev(first.getPrev().getPrev());
            first.getPrev().getNext().setPrev(null);
            first.getPrev().setNext(first);
        }
        
        size--;
        return true;
    }

    @Override
    public boolean remove(int i) throws ArrayIndexOutOfBoundsException{
        
        if(isEmpty() || i < 0 || i >= size)
            throw new ArrayIndexOutOfBoundsException();
        
        if(i == 0)
            return removeFirst();
        if(i == size - 1)
            return removeLast();
        
        Node<E> removedNode = getNodeByIndex(i);
        
        removedNode.getPrev().setNext(removedNode.getNext());
        removedNode.getNext().setPrev(removedNode.getPrev());
        removedNode.setNext(null);
        removedNode.setPrev(null);
        size--;
        return true;
    }

    @Override
    public boolean insert(E element, int i) throws ArrayIndexOutOfBoundsException{
        
        if(element == null)
            return false;
        if(i < 0 || i > size)
            throw new ArrayIndexOutOfBoundsException();
        
        if(i == 0)
            return addFirst(element);
        if(i == size)
            return addLast(element);
        
        Node<E> newNode = new Node<>(element);
        Node<E> iNode = getNodeByIndex(i);
        
        newNode.setNext(iNode);
        newNode.setPrev(iNode.getPrev());
        iNode.getPrev().setNext(newNode);
        iNode.setPrev(newNode);
        size++;
        return true;
    }

    @Override
    public boolean contains(E element) {
        
        if(element == null || isEmpty())
            return false;
        
        Node<E> leftTraveler = first;
        Node<E> rightTraveler = first.getPrev();
        
        do {
            
            if(leftTraveler.getData().equals(element) || rightTraveler.getData().equals(element))
                return true;
            
            leftTraveler = leftTraveler.getNext();
            rightTraveler = rightTraveler.getPrev();
        
        } while(rightTraveler != leftTraveler.getPrev() && rightTraveler.getNext() != leftTraveler.getPrev());
        
        return false;
    }

    @Override
    public E getElement(int i) throws ArrayIndexOutOfBoundsException{
        
        if(isEmpty() || i < 0 || i >= size)
            throw new ArrayIndexOutOfBoundsException();
        
        if(i == 0)
            return getFirst();
        if(i == size - 1)
            return getLast();
        
        return getNodeByIndex(i).getData();
    }

    @Override
    public E getFirst() {
        
        if(isEmpty())
            return null;
        
        return first.getData();
    }

    @Override
    public E getLast() {
        
        if(isEmpty())
            return null;
        
        return first.getPrev().getData();
    }
    
    private Node<E> getNodeByIndex(int i) throws ArrayIndexOutOfBoundsException{
        
        if(isEmpty() || i < 0 || i >= size)
            throw new ArrayIndexOutOfBoundsException();
        
        if(i == 0)
            return first;
        if(i == size - 1)
            return first.getPrev();
        
        int middle = (int)size/2;
        Node<E> iNode;
        
        if(i < middle){
            
            iNode = first.getNext();
            for(int n = 1; n < i; n++)
               iNode = iNode.getNext();
            
        } else{
            iNode = first.getPrev().getPrev();
            
            for(int n = size - 2; n > i; n--)
                iNode = iNode.getPrev();
        }
        
        return iNode;
    }

    @Override
    public Iterator<E> iterator() {
        
        Iterator<E> iterator = new Iterator<E>(){
            
            private Node<E> iNode = first;
            private int currentIndex = 0;
            
            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public E next() {
                
                E data = iNode.getData();
                iNode = iNode.getNext();
                currentIndex++;
                return data;
            }
            
        };
        
        return iterator;
    }
    
    @Override
    public boolean equals(Object o){
        
        if(o == null)
            return false;
        
        if(!(o instanceof DoublyLinkedCircularList))
            return false;
        
        DoublyLinkedCircularList<E> e = (DoublyLinkedCircularList<E>)o;
        
        if(this.size != e.size)
            return false;
        
        if(this.size == 0)
            return true;
        
        Node<E> thisNode = this.first;
        Node<E> eNode = e.first;
        
        do{
            if(!thisNode.getData().equals(eNode.getData()))
                return false;
            
            thisNode = thisNode.getNext();
            eNode = eNode.getNext();
            
        } while(thisNode != this.first);
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.first);
        return hash;
    }
    
}
